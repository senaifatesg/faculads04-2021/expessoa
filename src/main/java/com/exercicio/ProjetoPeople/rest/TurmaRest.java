package com.exercicio.ProjetoPeople.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exercicio.ProjetoPeople.entity.Turma;
import com.exercicio.ProjetoPeople.pdao.TurmaDao;

@RestController
@RequestMapping("/turma")
public class TurmaRest {

	@Autowired
	private TurmaDao turmaDao;
	
	@GetMapping
	public List<Turma> get(){
		return turmaDao.findAll();
	}
	
	@PostMapping
	public void post(@RequestBody Turma turma) {
		turmaDao.save(turma);
	}
	
	@DeleteMapping("/{id}")
	public void delete(Long id) {
		turmaDao.deleteById(id);
	}
}
