package com.exercicio.ProjetoPeople;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan


public class ProjetoPeopleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoPeopleApplication.class, args);
	}

}
