package com.exercicio.ProjetoPeople.pdao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercicio.ProjetoPeople.entity.Curso;

@Repository
public interface CursoDao extends JpaRepository<Curso, Long>{

}
