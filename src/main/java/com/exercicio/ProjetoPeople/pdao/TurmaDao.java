package com.exercicio.ProjetoPeople.pdao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercicio.ProjetoPeople.entity.Turma;

@Repository
public interface TurmaDao extends JpaRepository<Turma, Long>{
		
}