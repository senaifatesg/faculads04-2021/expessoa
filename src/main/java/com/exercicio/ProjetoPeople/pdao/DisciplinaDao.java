package com.exercicio.ProjetoPeople.pdao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercicio.ProjetoPeople.entity.Disciplina;

@Repository
public interface DisciplinaDao extends JpaRepository<Disciplina, Long>{

}
