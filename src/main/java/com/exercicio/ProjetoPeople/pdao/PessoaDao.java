package com.exercicio.ProjetoPeople.pdao;

import org.springframework.data.jpa.mapping.JpaPersistentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercicio.ProjetoPeople.entity.Pessoa;

@Repository
public interface PessoaDao extends JpaRepository<Pessoa, Long>{
		
}