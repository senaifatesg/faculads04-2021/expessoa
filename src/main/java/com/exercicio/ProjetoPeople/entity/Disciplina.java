package com.exercicio.ProjetoPeople.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Disciplina {

	 	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    private String nome;

	    @ManyToOne
	    @JoinColumn
	    private Turma turma;

	//    @OneToMany(mappedBy = "disciplina")
	//    private List<MatriculaDisciplinaPessoa> matriculaDaPessoas = new ArrayList<>();

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public Turma getTurma() {
			return turma;
		}

		public void setTurma(Turma turma) {
			this.turma = turma;
		}

/*
		public List<MatriculaDisciplinaPessoa> getMatriculaDaPessoas() {
			return matriculaDaPessoas;
		}

		public void setMatriculaDaPessoas(List<MatriculaDisciplinaPessoa> matriculaDaPessoas) {
			this.matriculaDaPessoas = matriculaDaPessoas;
		}
*/
		public Long getId() {
			return id;
		}

	    
}
