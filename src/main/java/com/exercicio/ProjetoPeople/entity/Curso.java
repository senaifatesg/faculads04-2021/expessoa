package com.exercicio.ProjetoPeople.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Curso {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

 //   @OneToMany(mappedBy = "curso")
 //   private List<Turma> turma = new ArrayList<>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
/*
	public List<Turma> getTurma() {
		return turma;
	}

	public void setTurma(List<Turma> turma) {
		this.turma = turma;
	}
*/
	public Long getId() {
		return id;
	}

    
}
