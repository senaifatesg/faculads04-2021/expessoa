package com.exercicio.ProjetoPeople.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class MatriculaDisciplinaPessoa {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn
    private Pessoa pessoa;

    @ManyToOne
    @JoinColumn
    private Disciplina disciplina;

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public Long getId() {
		return id;
	}

    
}
